# coding=utf-8
import octoprint.plugin
from octoprint.events import Events
from sense_hat import SenseHat

import math

class ProgressOnSenseHatPlugin(octoprint.plugin.ProgressPlugin):

    def on_print_progress(self, storage, path, progress):
        GREEN_TEXT = [0, 255, 0]
        GREEN_BACK = [0, 50, 0]
        RED_TEXT = [255, 0, 0]
        RED_BACK = [50, 0, 0]
        NUMBER_OF_LEDS = 64
        NUMBERS = [[## 0 ##
                    [0,0,0],
                    [1,1,1],
                    [1,0,1],
                    [1,0,1],
                    [1,0,1],
                    [1,0,1],
                    [1,1,1],
                    [0,0,0]],
                   [## 1 ##
                    [0,0,0],
                    [0,1,0],
                    [0,1,0],
                    [0,1,0],
                    [0,1,0],
                    [0,1,0],
                    [0,1,0],
                    [0,0,0]],
                   [## 2 ##
                    [0,0,0],
                    [1,1,1],
                    [0,0,1],
                    [1,1,1],
                    [1,0,0],
                    [1,0,0],
                    [1,1,1],
                    [0,0,0]],
                   [## 3 ##
                    [0,0,0],
                    [1,1,1],
                    [0,0,1],
                    [0,1,1],
                    [0,0,1],
                    [0,0,1],
                    [1,1,1],
                    [0,0,0]],
                   [## 4 ##
                    [0,0,0],
                    [1,0,0],
                    [1,0,1],
                    [1,1,1],
                    [0,0,1],
                    [0,0,1],
                    [0,0,1],
                    [0,0,0]],
                   [## 5 ##
                    [0,0,0],
                    [1,1,1],
                    [1,0,0],
                    [1,1,1],
                    [0,0,1],
                    [0,0,1],
                    [1,1,1],
                    [0,0,0]],
                   [## 6 ##
                    [0,0,0],
                    [1,0,0],
                    [1,0,0],
                    [1,0,0],
                    [1,1,1],
                    [1,0,1],
                    [1,1,1],
                    [0,0,0]],
                   [## 7 ##
                    [0,0,0],
                    [1,1,1],
                    [0,0,1],
                    [0,0,1],
                    [0,1,0],
                    [0,1,0],
                    [0,1,0],
                    [0,0,0]],
                   [## 8 ##
                    [0,0,0],
                    [1,1,1],
                    [1,0,1],
                    [1,1,1],
                    [1,0,1],
                    [1,0,1],
                    [1,1,1],
                    [0,0,0]],
                   [## 9 ##
                    [0,0,0],
                    [1,1,1],
                    [1,0,1],
                    [1,1,1],
                    [0,0,1],
                    [0,0,1],
                    [0,0,1],
                    [0,0,0]]]

        sense = SenseHat()

        progress_array = []
        for x in range(NUMBER_OF_LEDS):
            if (x*100/NUMBER_OF_LEDS) < progress:
                progress_array.append(GREEN_BACK)
            else:
                progress_array.append(RED_BACK)

        for digits in range(2):
            if digits == 0:
                offset = 0
                number = int(int(progress // 10)%10)
            else:
                offset = 5
                number = int(progress % 10)
            for x in range(3):
                for y in range(8):
                    if ((offset == 0) and (number == 0)) or (progress == 100): break
                    color = progress_array[x+offset+(y*8)]
                    if NUMBERS[number][y][x]:
                        if color == RED_BACK:
                            progress_array[x+offset+(y*8)]=RED_TEXT
                        if color == GREEN_BACK:
                            progress_array[x+offset+(y*8)]=GREEN_TEXT

        sense.set_pixels(progress_array)

    def get_version(self):
        return self._plugin_version

__plugin_pythoncompat__ = ">=2.7,<4"
__plugin_implementation__ = ProgressOnSenseHatPlugin()
