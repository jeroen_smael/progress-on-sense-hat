# Progress-On-Sense-Hat

This plugin shows the print progress on a Raspberry Pi Sense-Hat.  
You have to 'install' the Sense-Hat hardware and install the Sense-Hat software. After this, install this plugin and enjoy :-)  
There is nothing (yet) to configure on the plugin. 

![alt text](ProgressOnSenseHat.jpg "Example of the plugin")

## Setup

Install via the bundled [Plugin Manager](https://docs.octoprint.org/en/master/bundledplugins/pluginmanager.html)
or manually using this URL:

    https://bitbucket.org/jeroen_smael/progress-on-sense-hat/get/master.zip

As already mention, you will also have to install the Sense-Hat software **first**.  
Connect to OctoPrint via ssh (usually 'ssh octopi.local') and login.  
After this, do:  
  * sudo apt update  
  * sudo apt install libatlas-base-dev  
  * sudo adduser pi i2c  
  * source ~/oprint/bin/activate  
  * pip install sense-hat  
  * pip install RTIMULib  
Don't forget to activate i2c using raspi-config!
After this, install this plugin (you can do this in the plugin manager by using the link mentioned above).

## Configuration

No configuration is required (yet)
